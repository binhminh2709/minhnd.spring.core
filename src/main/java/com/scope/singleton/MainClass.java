package com.scope.singleton;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainClass {
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("scope/singleton/Beans.xml");
		Amount Firstamt = (Amount) context.getBean("amount");
		/* instance of bean returned*/
		
		Firstamt.setVal("SimpleCodeStuffs");
		
		System.out.println(Firstamt.getVal());
		
		Amount Secondamt = (Amount) context.getBean("amount");
		/*For singleton scope, new bean instance
		is not returned at this point. Hence the same value is printed. */
		System.out.println(Secondamt.getVal());
		
		/**
		 * Note that, as the bean is of singleton scope, only one instance is returned for
		 * the beanID amount per IOC container. This can be seen in the output as the same value
		 * is printed twice, once for Firstamt and once for Secondamt.
		 * */
	}
}
