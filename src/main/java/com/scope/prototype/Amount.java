package com.scope.prototype;

public class Amount {
	
	private String	val;
	
	public String getVal() {
		return val;
	}
	
	public void setVal(String val) {
		this.val = val;
	}
	
	/**
	 * In prototype scope,a new instance of the bean is returned per request. (every time
	 * getBean() method is called)
	 * */
}
