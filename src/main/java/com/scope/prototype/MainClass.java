package com.scope.prototype;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainClass {
	
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("scope/prototype/Beans.xml");
		Amount Firstamt = (Amount) context.getBean("amount");
		/* instance of bean returned*/
		Firstamt.setVal("SimpleCodeStuffs");
		
		System.out.println(Firstamt.getVal());
		
		Amount Secondamt = (Amount) context.getBean("amount");
		/*For PROTOTYPE scope, new bean instance is returned */
		
		System.out.println(Secondamt.getVal());
		
/**
 * Note that the bean is of scope "prototype". So an instance of the bean is returned
 * everytime a request is made using getBean() method. So, a new instance is returned for
 * Secondamt and no value has been set for the new instance.
 * This can be seen in output as Secondamt prints null value.
 * */
	}
}
