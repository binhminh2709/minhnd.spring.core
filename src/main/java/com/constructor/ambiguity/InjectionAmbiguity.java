package com.constructor.ambiguity;

public class InjectionAmbiguity {
	
	private String	name;
	private String	score;
	private int			age;
	
	//constructor 1
	public InjectionAmbiguity(String name, String score, int age) {
		this.name = name;
		this.age = age;
		this.score = score;
	}
	
	//constructor 2
	public InjectionAmbiguity(String name, int age, String score) {
		this.name = name;
		this.age = age;
		this.score = score;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getAge() {
		return age;
	}
	
	public void setAge(int age) {
		this.age = age;
	}
	
	public String getScore() {
		return score;
	}
	
	public void setScore(String score) {
		this.score = score;
	}
}
