package com.collection;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class CollectionBean {
	private List<String>				stringListVariable;
	private List<Object>				listVariable;
	private Set<Object>					setVariable;
	private Map<Object, Object>	mapVariable;
	private Properties					propVariable;
	
	public List<String> getStringListVariable() {
		return stringListVariable;
	}
	
	public void setStringListVariable(List<String> stringListVariable) {
		this.stringListVariable = stringListVariable;
	}
	
	public List<Object> getListVariable() {
		return listVariable;
	}
	
	public void setListVariable(List<Object> listVariable) {
		this.listVariable = listVariable;
	}
	
	public Set<Object> getSetVariable() {
		return setVariable;
	}
	
	public void setSetVariable(Set<Object> setVariable) {
		this.setVariable = setVariable;
	}
	
	public Map<Object, Object> getMapVariable() {
		return mapVariable;
	}
	
	public void setMapVariable(Map<Object, Object> mapVariable) {
		this.mapVariable = mapVariable;
	}
	
	public Properties getPropVariable() {
		return propVariable;
	}
	
	public void setPropVariable(Properties propVariable) {
		this.propVariable = propVariable;
	}
}
/**
CollectionBean is a class, which has a list,set,map,properties of another Object as its
attributes. Just to enhance the understanding, we take it that CollectionBean may have
a List of any Object as its attributes. Hence the attributes are defined as List of Object
and likewise.
 * */
