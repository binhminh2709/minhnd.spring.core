package com.collection;

public class Amount {
	
	private double	bill;
	
	public double getBill() {
		return bill;
	}
	
	public void setBill(double bill) {
		this.bill = bill;
	}
	
	@Override
	public String toString() {
		return ("amount " + bill);
	}
}
