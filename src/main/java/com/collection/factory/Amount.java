package com.collection.factory;

public class Amount {
	private String	val;
	
	public String getVal() {
		return val;
	}
	
	public void setVal(String val) {
		this.val = val;
	}
	
	public String toString() {
		return ("amount bean val: " + val);
	}
}
