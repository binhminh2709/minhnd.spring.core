package com.spEL.accessing;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainClass {
	
	public static void main(String[] args) {
		
		ApplicationContext context = new ClassPathXmlApplicationContext("spEL/elBeansAccessing.xml");
		Customer customer = (Customer) context.getBean("cust");
		
		System.out.println("Customer name  : " + customer.getName());
		System.out.println("Bill price     : " + customer.getBill());
	}
}
