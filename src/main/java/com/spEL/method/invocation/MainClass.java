package com.spEL.method.invocation;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainClass {
	
	public static void main(String[] args) {
		
		ApplicationContext context = new ClassPathXmlApplicationContext("spEL/elBeansMethodInvocation.xml");
		Amount bill = (Amount) context.getBean("amount");
		Customer customer = (Customer) context.getBean("cust");
		
		System.out.println("Amount         : " + bill.billAmount());
		
		System.out.println("Customer name  : " + customer.getName());
		System.out.println("Bill amount    : " + customer.getBill());
	}
	
}
