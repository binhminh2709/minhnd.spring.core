package com.spEL.method.invocation;

public class Customer {
	
	private String	name;
	private double	bill;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public double getBill() {
		return bill;
	}
	
	public void setBill(double bill) {
		this.bill = bill;
	}
}
