package com.spEL.method.invocation;

public class Amount {
	
	public double billAmount() {
		return 100.9;
	}
	
/**
 * The scenario is such that the value of bill attribute of 'Customer' depends on the value
 * returned from ‘billAmount()’ method of ‘Amount’
 * */
}
