package com.spEL.operators;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("operators")
public class Operators {
	
	@Value("#{1==1}")
	private boolean	equals;
	
	public boolean isEquals() {
		return equals;
	}
	
	public void setEquals(boolean equals) {
		this.equals = equals;
	}
	
	@Value("#{1 != 5}")
	//true
	private boolean	NotEqual;
	
	@Value("#{1 < 1}")
	//false
	private boolean	LessThan;
	
	@Value("#{1 <= 1}")
	//true
	private boolean	LessThanOrEqual;
	
	@Value("#{1 > 1}")
	//false
	private boolean	GreaterThan;
	
	@Value("#{1 >= 1}")
	//true
	private boolean	GreaterThanOrEqual;
	
	/*
	 * Logical operators , numberBean.no == 999
	 * @Value("#{numberBean.no == 999 and numberBean.no < 900}") 
	 * can be used in case 'numberBean' is another bean.
	 */
	
	@Value("#{999 == 999 and 999 < 900}")
	//false
	private boolean	And;
	
	@Value("#{999 == 999 or 999 < 900}")
	//true
	private boolean	Or;
	
	@Value("#{!(999 == 999)}")
	//false
	private boolean	Not;
	
	//Mathematical operators
	
	@Value("#{1 + 1}")
	//2.0
	private double	testAdd;
	
	@Value("#{'1' + '@' + '1'}")
	//1@1
	private String	AddString;
	
	@Value("#{1 - 1}")
	//0.0
	private double	Subtraction;
	
	@Value("#{1 * 1}")
	//1.0
	private double	Multiplication;
	
	@Value("#{10 / 2}")
	//5.0
	private double	Division;
	
	@Value("#{10 % 10}")
	//0.0
	private double	Modulus;
	
	@Value("#{2 ^ 2}")
	//4.0
	private double	ExponentialPower;
	
	//Ternary operator
	@Value("#{99!=0?'expresion holds true':'expression is false'}")
	private String	name;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
}
