package com.spEL.operators;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainClass {
	
	public static void main(String[] args) {
		
		ApplicationContext context = new ClassPathXmlApplicationContext("spEL/elBeansOperators.xml");
		Operators op = (Operators) context.getBean("operators");
		System.out.println("=======equals=======" + op.isEquals());
		
		System.out.println("=======NAME=======" + op.getName());
	}
}
