package com.spEL.base;

public class Address {
	private String	doorNo;
	private String	street;
	private String	area;
	
	public Address() {
		this.doorNo = null;
		this.street = null;
		this.area = null;
	}
	
	public Address(String doorNo, String street, String area) {
		this.doorNo = doorNo;
		this.street = street;
		this.area = area;
	}
	
	public String getArea() {
		return area;
	}
	
	public void setArea(String area) {
		this.area = area;
	}
	
	public String getDoorNo() {
		return doorNo;
	}
	
	public void setDoorNo(String doorNo) {
		this.doorNo = doorNo;
	}
	
	public String getStreet() {
		return street;
	}
	
	public void setStreet(String street) {
		this.street = street;
	}
}
