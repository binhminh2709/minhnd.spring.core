package com.spEL.anonation;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainClass {
	
	public static void main(String[] args) {
		
		ApplicationContext context = new ClassPathXmlApplicationContext("spEL/elBeansAnonation.xml");
		Candidate can = (Candidate) context.getBean("elCandidate");
		Address add = (Address) context.getBean("elAddress");
		
		System.out.println("Name    : " + can.getName());
		System.out.println("Age     : " + can.getAge());
		System.out.println("Area    : " + can.getArea());
		System.out.println("Address : " + can.getAddrs().getDoorNo());
		System.out.println("Street  : " + add.getStreet());
		System.out.println("Area    : " + add.getArea());
	}
}
