package com.bean.lifecycle;

public class Amount {

    private String val;

    public String getVal() { return val; }

    public void setVal(String val) { this.val = val; }

    public String toString() { return ("amount bean val: " + val); }

    public void startUp() {
        System.out.println("init() method called after properties to Amount are set. ");
        System.out.println(toString());
    }

    public void cleanUp() {
        System.out.println("destroy method called when application context is closed");
        System.out.println("Releasing/destroying all resources");
        System.out.println(toString());
    }


}
