
package com.bean.annotation;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {
	
	public static void main(String[] args) {
		
		//ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
		//HelloWorld obj = (HelloWorld) context.getBean("helloBean");
		//obj.printHelloWorld("Spring3 Java Config");
		
		
		ApplicationContext context = new AnnotationConfigApplicationContext(ImportConfig.class);
		CustomerBo customer = (CustomerBo) context.getBean("customer");
		customer.printMsg("Hello 1");
 
		SchedulerBo scheduler = (SchedulerBo) context.getBean("scheduler");
		scheduler.printMsg("Hello 2");
	}
}
