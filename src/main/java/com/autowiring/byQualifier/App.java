
package com.autowiring.byQualifier;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
	
	public static void main(String[] args) {
		
		//ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
		//HelloWorld obj = (HelloWorld) context.getBean("helloBean");
		//obj.printHelloWorld("Spring3 Java Config");
		
		ApplicationContext context = new ClassPathXmlApplicationContext(new String[] { "autowiring/byQualifier.xml" });
		Customer cus = (Customer) context.getBean("customer");
		System.out.println(cus.getPerson().getName());
		
	}
}
