package com.loosely.coupled;

public interface IOutputGenerator {
	public void generateOutput();
}
