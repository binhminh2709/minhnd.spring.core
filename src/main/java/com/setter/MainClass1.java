package com.setter;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainClass1 {
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("BeansSetter.xml");
		SimpleSetterInjection obj = (SimpleSetterInjection) context.getBean("setterInjection");
		obj.getMessage();
	}
}
