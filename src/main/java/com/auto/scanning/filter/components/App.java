
package com.auto.scanning.filter.components;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.auto.scanning.filter.components.services.CustomerService;

public class App {
	
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext(new String[] {
			"AutoScanningComponents/filterComponents.xml"
		});
		
		CustomerService cust = (CustomerService) context.getBean("customerService");
		System.out.println(cust);
		
	}
}
